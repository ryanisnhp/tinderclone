//
//  LoadFBImagesViewController.swift
//  TinderClone
//
//  Created by admin on 5/9/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import FBSDKCoreKit
class LoadFBImagesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    // MARK: *** Local variables
    let graphRequestParameters = ["fields": "id,name,cover_photo{images},photo_count,updated_time"]
    var alertController: UIAlertController? = nil
    var arrAlbumId: Array<String> = []
    var arrAlbumName: Array<String> = []
    var arrAlbumCoverId: Array<String> = []
    var arrAlbumCoverImages: Array<String> = []
    var arrAlbumPhotosCount: Array<Int> = []
    var arrAlbumUpdatedTime: Array<String> = []
    // MARK: *** Dataum Models
    
    // MARK: *** UI Elements
    @IBOutlet weak var albumTableView: UITableView!
    
    
    // MARK: *** UI Events
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrAlbumCoverId.removeAll()
        arrAlbumId.removeAll()
        arrAlbumName.removeAll()
        arrAlbumCoverImages.removeAll()
        arrAlbumPhotosCount.removeAll()
        arrAlbumUpdatedTime.removeAll()
        
        self.albumTableView.tableFooterView = UIView(frame: CGRect.zero)  // remove divider

        graphRequestToReturnUserData(graphParameters: graphRequestParameters)
    }

    //Mark: GraphRequestForFetchingUserData
    func graphRequestToReturnUserData(graphParameters : Dictionary<String, String>) {
        showDialogWaiting()
        FBSDKGraphRequest(graphPath: "me/albums", parameters: graphParameters).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                let data = fbDetails["data"] as! [AnyObject]
                for i in 0...data.count-1{
                    let idAlbums = data[i]["id"] as! String
                    let nameAlbums = data[i]["name"] as! String
                    let coverPhotos = data[i]["cover_photo"] as AnyObject
                    let updatedTimeAlbums = data[i]["updated_time"] as! String
                    let idCoverPhoto = coverPhotos["id"] as! String
                    let imageCoverPhoto = coverPhotos["images"] as! [AnyObject]
                    let imageCoverPhotoSource = imageCoverPhoto[1]["source"] as! String
                    
                    let photosCount = data[i]["photo_count"] as! Int
                    
                    self.arrAlbumId.append(idAlbums)
                    self.arrAlbumCoverId.append(idCoverPhoto)
                    self.arrAlbumCoverImages.append(imageCoverPhotoSource)
                    self.arrAlbumName.append(nameAlbums)
                    self.arrAlbumPhotosCount.append(photosCount)
                    self.arrAlbumUpdatedTime.append(updatedTimeAlbums)
                    //dismiss waiting dialog
                    self.alertController?.dismiss(animated: true, completion: nil)
                    
                    self.albumTableView.delegate = self
                    self.albumTableView.dataSource = self
                    self.albumTableView.reloadData()
                }
            }
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("View will apprear")
        albumTableView.reloadData() // update UI
    }
    override func viewDidAppear(_ animated: Bool) {
        print("View did appear")
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToImageCollection" {
            let dest = segue.destination as! ListPhotosViewController
            dest.idAlbum = self.selectedAlbum
            //dest.delegateListAlbum = self
        }
    }

    
    func showDialogWaiting() {
        alertController = UIAlertController(title: nil, message: "Please wait\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        alertController?.view.addSubview(spinnerIndicator)
        self.present(alertController!, animated: false, completion: nil)
    }
    
    // MARK: *** UITableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //return number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAlbumCoverId.count
    }
    //set data for each row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumCellId") as! AlbumTableViewCell
        
        //set album cover
        let url = URL(string: arrAlbumCoverImages[indexPath.row])
        let data = try? Data(contentsOf: url!)
        let uiImage = UIImage(data: data!)
        cell.imageAlbumCover.image = uiImage
        
        cell.albumName.text = arrAlbumName[indexPath.row]
        if arrAlbumPhotosCount[indexPath.row] > 1{
            cell.albumPhotoCounts.text = String(arrAlbumPhotosCount[indexPath.row]) + " pictures"
        }else{
            cell.albumPhotoCounts.text = String(arrAlbumPhotosCount[indexPath.row]) + " picture"
        }

        
        return cell
    }
    var selectedAlbum : String? = nil
    var selectedIdImageView: Int? = nil
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIdImageView = indexPath.row
        selectedAlbum = arrAlbumId[indexPath.row]
        performSegue(withIdentifier: "segueToImageCollection", sender: self)
    }
}
