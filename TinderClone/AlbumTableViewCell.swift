//
//  AlbumTableViewCell.swift
//  TinderClone
//
//  Created by admin on 5/10/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {

    // MARK: *** Local variables
    
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    @IBOutlet weak var imageAlbumCover: UIImageView!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var albumPhotoCounts: UILabel!
    
    // MARK: *** UI Events
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageAlbumCover.layer.masksToBounds = true
        imageAlbumCover.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
