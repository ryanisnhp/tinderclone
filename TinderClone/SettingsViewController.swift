//
//  SettingsViewController.swift
//  TinderClone
//
//  Created by admin on 5/8/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import DropDown
import FBSDKCoreKit
protocol SettingsControllerDelegate {
    func setImage(newImage: String)
}
class SettingsViewController: UIViewController, SettingsControllerDelegate {
    func setImage(newImage: String) {
        //set image
        print("URL IMAGE : " + newImage)
        //UserDefaults.standard.set(newImage, forKey: "url")
        if UserDefaults.standard.integer(forKey: "index") >= 0{
            var currentIndex = UserDefaults.standard.integer(forKey: "index")
            switch currentIndex {
            case 0:
                UserDefaults.standard.set(newImage, forKey: "index_0")
            case 1:
                UserDefaults.standard.set(newImage, forKey: "index_1")
            case 2:
                UserDefaults.standard.set(newImage, forKey: "index_2")
            case 3:
                UserDefaults.standard.set(newImage, forKey: "index_3")
            case 4:
                UserDefaults.standard.set(newImage, forKey: "index_4")
            case 5:
                UserDefaults.standard.set(newImage, forKey: "index_5")
            default:
                break
            }
            
            currentIndex = (currentIndex + 1)%6
            UserDefaults.standard.set(currentIndex, forKey: "index")
            
            
        }else{
            UserDefaults.standard.set(0, forKey: "index")
            UserDefaults.standard.set(newImage, forKey: "index_0")
        }
    }


    // MARK: *** Local variables
    let graphRequestParameters = ["fields": "id,name,education,about,birthday,email,age_range,work,relationship_status"]
    var tappedImage:UIImageView? = nil
    var alertController: UIAlertController? = nil
    let chooseWorkDropDown = DropDown()
    let chooseEducationDropDown = DropDown()
    var arrSchools: Array<String> = []
    var arrJobs: Array<String> = []
    var relationship: String? = nil
    var dob: String? = nil
    var aboutInfo: String? = nil
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img4: UIImageView!
    @IBOutlet weak var img5: UIImageView!
    @IBOutlet weak var img6: UIImageView!
    

    @IBOutlet weak var dropdownWork: UIButton!
    
    @IBOutlet weak var dropdownEducation: UIButton!
    
    @IBOutlet weak var relationshipStatus: UILabel!

    @IBOutlet weak var birthday: UILabel!
    
    @IBOutlet weak var about: UITextView!
    
    // MARK: *** UI Events
    
    @IBAction func dropdownWorkTapped(_ sender: Any) {
        chooseWorkDropDown.show()
        // Action triggered on selection
        chooseWorkDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.dropdownWork.setTitle(item, for: .normal)
        }
    }
    
    @IBAction func dropdownEducationTapped(_ sender: Any) {
        chooseEducationDropDown.show()
        // Action triggered on selection
        chooseEducationDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.dropdownEducation.setTitle(item, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrSchools.removeAll()
        arrJobs.removeAll()
        
        graphRequestToReturnUserData(graphParameters: graphRequestParameters)
        
        addImageGesture()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let getURL0 = UserDefaults.standard.value(forKey: "index_0"){
            //set image
            img1.image = urlToData(url: getURL0 as! String)
        }
        if let getURL1 = UserDefaults.standard.value(forKey: "index_1"){
            //set image
            img2.image = urlToData(url: getURL1 as! String)
        }
        if let getURL2 = UserDefaults.standard.value(forKey: "index_2"){
            //set image
            img3.image = urlToData(url: getURL2 as! String)
        }
        if let getURL3 = UserDefaults.standard.value(forKey: "index_3"){
            //set image
            img4.image = urlToData(url: getURL3 as! String)
        }
        if let getURL4 = UserDefaults.standard.value(forKey: "index_4"){
            //set image
            img5.image = urlToData(url: getURL4 as! String)
        }
        if let getURL5 = UserDefaults.standard.value(forKey: "index_5"){
            //set image
            img6.image = urlToData(url: getURL5 as! String)
        }
    }
    func urlToData(url: String) -> UIImage {
        let url = URL(string: url)
        let data = try? Data(contentsOf: url!)
        let uiImage = UIImage(data: data!)
        return uiImage!
    }
    
    //Mark: GraphRequestForFetchingUserData
    func graphRequestToReturnUserData(graphParameters : Dictionary<String, String>) {
        showDialogWaiting()
        FBSDKGraphRequest(graphPath: "me", parameters: graphParameters).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                //get education info
                let education = fbDetails["education"] as! [AnyObject]
                for i in 0...education.count-1{
                    let school = education[i]["school"] as AnyObject
                    let schoolName = school["name"] as! String
                    self.arrSchools.append(schoolName)
                }
                //get work info
                let work = fbDetails["work"] as! [AnyObject]
                for i in 0...work.count-1{
                    let employer = work[i]["employer"] as AnyObject
                    let name = employer["name"] as! String
                    self.arrJobs.append(name)
                }
                self.dob = fbDetails["birthday"] as? String
                self.aboutInfo = fbDetails["about"] as? String
                self.relationship = fbDetails["relationship_status"] as? String
                
                self.relationshipStatus.text = self.relationship
                self.birthday.text = self.dob
                self.about.text = self.aboutInfo
                
                
                self.chooseEducationDropDown.anchorView = self.dropdownEducation // UIView or UIBarButtonItem
                self.chooseEducationDropDown.dataSource = self.arrSchools
                self.chooseWorkDropDown.anchorView = self.dropdownWork // UIView or UIBarButtonItem
                self.chooseWorkDropDown.dataSource = self.arrJobs
                self.chooseEducationDropDown.reloadAllComponents()
                self.chooseWorkDropDown.reloadAllComponents()
                
                self.alertController?.dismiss(animated: true, completion: nil)
            }
        })
    }
    func showDialogWaiting() {
        alertController = UIAlertController(title: nil, message: "Please wait\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        alertController?.view.addSubview(spinnerIndicator)
        self.present(alertController!, animated: false, completion: nil)
    }

    func addImageGesture(){
        img1.layer.masksToBounds = true
        img1.layer.cornerRadius = 10.0
        img2.layer.masksToBounds = true
        img2.layer.cornerRadius = 10.0
        img3.layer.masksToBounds = true
        img3.layer.cornerRadius = 10.0
        img4.layer.masksToBounds = true
        img4.layer.cornerRadius = 10.0
        img5.layer.masksToBounds = true
        img5.layer.cornerRadius = 10.0
        img6.layer.masksToBounds = true
        img6.layer.cornerRadius = 10.0
        
        let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer5 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        let tapGestureRecognizer6 = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        img1.isUserInteractionEnabled = true
        img1.addGestureRecognizer(tapGestureRecognizer1)
        img2.isUserInteractionEnabled = true
        img2.addGestureRecognizer(tapGestureRecognizer2)
        img3.isUserInteractionEnabled = true
        img3.addGestureRecognizer(tapGestureRecognizer3)
        img4.isUserInteractionEnabled = true
        img4.addGestureRecognizer(tapGestureRecognizer4)
        img5.isUserInteractionEnabled = true
        img5.addGestureRecognizer(tapGestureRecognizer5)
        img6.isUserInteractionEnabled = true
        img6.addGestureRecognizer(tapGestureRecognizer6)
    }
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        tappedImage = tapGestureRecognizer.view as? UIImageView
        switch tappedImage!.tag {
        case 1:
            print(tappedImage!.tag)
            performSegue(withIdentifier: "segueToLoadFBImages", sender: self)
        case 2:
            print(tappedImage!.tag)
            performSegue(withIdentifier: "segueToLoadFBImages", sender: self)
        case 3:
            print(tappedImage!.tag)
            performSegue(withIdentifier: "segueToLoadFBImages", sender: self)
        case 4:
            print(tappedImage!.tag)
            performSegue(withIdentifier: "segueToLoadFBImages", sender: self)
        case 5:
            print(tappedImage!.tag)
            performSegue(withIdentifier: "segueToLoadFBImages", sender: self)
        case 6:
            print(tappedImage!.tag)
            performSegue(withIdentifier: "segueToLoadFBImages", sender: self)
        default:
            print("Nothing")
        }
        // Your action
    }
}
