//
//  HomeViewController.swift
//  TinderClone
//
//  Created by admin on 5/8/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import Koloda
import pop
import FBSDKCoreKit
import FBSDKLoginKit

private var numberOfCards: Int = 1
private let frameAnimationSpringBounciness: CGFloat = 9
private let frameAnimationSpringSpeed: CGFloat = 16
private let kolodaCountOfVisibleCards = 2
private let kolodaAlphaValueSemiTransparent: CGFloat = 0.1

class HomeViewController: UIViewController {
    // MARK: *** Local variables
    let graphRequestParameters = ["fields": "id,name,picture.width(480).height(480)"]
    var arrPictures: Array<String> = []
    var arrWidth: Array<Int> = []
    var arrNames: Array<String> = []
    var pageBefore : String = ""
    var pageAfter : String = ""
    var alertController: UIAlertController? = nil
    
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    @IBOutlet weak var kolodaView: KolodaView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    // MARK: *** UI Events
    
    @IBAction func leftButtonTapped(_ sender: Any) {
        kolodaView?.swipe(.left)
    }
    
    @IBAction func rightButtonTapped(_ sender: Any) {
        kolodaView?.swipe(.right)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        kolodaView.dataSource = nil
        
        leftButton.isHidden = true
        rightButton.isHidden = true
        // Get request data
        //showDialogWaiting()
        graphRequestToReturnUserData(graphParameters: graphRequestParameters)
        //dismissDialogWaiting()
        
        kolodaView.countOfVisibleCards = kolodaCountOfVisibleCards
        kolodaView.animator = BackgroundKolodaAnimator(koloda: kolodaView)
        modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
        self.kolodaView.alphaValueSemiTransparent = kolodaAlphaValueSemiTransparent
    }
    
    //Mark: GraphRequestForFetchingUserData
    func graphRequestToReturnUserData(graphParameters : Dictionary<String, String>) {
        //get 25 friends for each page
        FBSDKGraphRequest(graphPath: "me/taggable_friends", parameters: graphParameters).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                
                let fbDetails = result as! NSDictionary
                let arrData = fbDetails["data"] as! [AnyObject]
                print("arrData: " + String(arrData.count))
                numberOfCards = arrData.count
                
                self.arrNames.removeAll()
                self.arrPictures.removeAll()
                self.arrWidth.removeAll()
                for i in 0 ..< arrData.count{
                    let name = arrData[i]["name"]! as! String
                    let pic = arrData[i]["picture"] as AnyObject
                    let picData = pic["data"] as AnyObject
                    let url = picData["url"]
                    let width = picData["width"] as! Int
                    self.arrNames.append(name)
                    self.arrWidth.append(width)
                    self.arrPictures.append(url!! as! String)
                }
                //print(FBSDKAccessToken.current().tokenString)
                let paging = fbDetails["paging"] as AnyObject
                let cursors = paging["cursors"] as AnyObject
                
                //Get page before and page after
                let before = cursors["before"] as! String
                let after = cursors["after"] as! String
                self.pageBefore = before
                self.pageAfter = after
                
                //Delegate and datasource to load arrPicture in card
                self.kolodaView.delegate = self
                self.kolodaView.dataSource = self
                
                
//                self.kolodaView.countOfVisibleCards = kolodaCountOfVisibleCards
//                self.kolodaView.animator = BackgroundKolodaAnimator(koloda: self.kolodaView)
//                self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
//                self.kolodaView.alphaValueSemiTransparent = kolodaAlphaValueSemiTransparent
                
                //show buttons
                self.leftButton.isHidden = false
                self.rightButton.isHidden = false
            }
        })
    }
    func showDialogWaiting() {
        print("Show dialog")
        alertController = UIAlertController(title: nil, message: "Please wait\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        alertController?.view.addSubview(spinnerIndicator)
        self.present(alertController!, animated: false, completion: nil)
    }
    func dismissDialogWaiting(){
        self.alertController?.dismiss(animated: true, completion: nil)
        print("Dismiss dialog")
    }
}


//MARK: KolodaViewDelegate
extension HomeViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        koloda.dataSource = nil
        //reset arrPictures after out of cards
        arrNames.removeAll()
        arrWidth.removeAll()
        arrPictures.removeAll()
        
        //Continue requesting new page
        let graphRequestParametersTemp = ["fields": "id,name,picture.width(400).height(400)", "after": pageAfter]
        graphRequestToReturnUserData(graphParameters: graphRequestParametersTemp)
        
        //back to card with index 0
        kolodaView.resetCurrentCardIndex()
    }
    
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        //UIApplication.shared.openURL(URL(string: "https://yalantis.com/")!)
    }
    
    func kolodaShouldApplyAppearAnimation(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func kolodaShouldMoveBackgroundCard(_ koloda: KolodaView) -> Bool {
        return false
    }
    
    func kolodaShouldTransparentizeNextCard(_ koloda: KolodaView) -> Bool {
        return true
    }
    
    func koloda(kolodaBackgroundCardAnimation koloda: KolodaView) -> POPPropertyAnimation? {
        let animation = POPSpringAnimation(propertyNamed: kPOPViewFrame)
        animation?.springBounciness = frameAnimationSpringBounciness
        animation?.springSpeed = frameAnimationSpringSpeed
        return animation
    }
}

// MARK: KolodaViewDataSource
extension HomeViewController: KolodaViewDataSource {
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        
        print("numberOfCards: " + String(numberOfCards))
        return numberOfCards
    }
    
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        //load 25 images for each pages
        
        print(index)
        print(arrWidth[index])
        print(arrPictures[index])
        let url = URL(string: arrPictures[index])
        let data = try? Data(contentsOf: url!)
        let uiImage = UIImage(data: data!)
        
        let x = Float(arrWidth[index]) * 0.1
        let y = Float(arrWidth[index]) * 0.8
        let point = CGPoint(x: CGFloat(x), y: CGFloat(y))
        let image = UIImageView(image: uiImage?.textToImage(drawText: arrNames[index] as NSString, inImage: uiImage!, atPoint: point, withSize: (Float(arrWidth[index])*0.08)))
        image.contentMode = .scaleAspectFit
        image.layer.masksToBounds = true
        image.layer.cornerRadius = 15.0
        return image
    }
    
    func koloda(_ koloda: KolodaView, viewForCardOverlayAt index: Int) -> OverlayView? {
        return Bundle.main.loadNibNamed("CustomOverlayView", owner: self, options: nil)?[0] as? OverlayView
    }
}
