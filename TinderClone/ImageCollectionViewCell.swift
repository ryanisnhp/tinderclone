//
//  ImageCollectionViewCell.swift
//  TinderClone
//
//  Created by admin on 5/10/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    // MARK: *** Local variables
    
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    @IBOutlet weak var image: UIImageView!
    // MARK: *** UI Events
    override func awakeFromNib() {
        super.awakeFromNib()
        image.layer.masksToBounds = true
        image.layer.cornerRadius = 10.0
    }
}
