//
//  ViewController.swift
//  TinderClone
//
//  Created by admin on 5/6/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FacebookLogin

class LoginController: UIViewController, FBSDKLoginButtonDelegate{

    // MARK: *** Local variables
    let permissionsToRead = ["public_profile", "email", "user_birthday","user_friends", "user_work_history",
                             "user_education_history", "user_photos","user_relationships", "user_about_me"]
//    let graphRequestParameters = ["fields": "name, birthday, first_name, last_name, gender, relationship_status, email, work, education, photos"]
    let graphRequestParameters = ["fields": "work, education"]
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    @IBOutlet weak var loginFacebook: FBSDKLoginButton!
    
    @IBOutlet weak var goSwipping: UIButton!
    
    // MARK: *** UI Events
    
    @IBAction func loginFacebookButton(_ sender: Any) {
        self.loginFacebook.delegate = self
    }
    
    @IBAction func swippingButtonTapped(_ sender: Any) {
        FBSDKAppEvents.logEvent("continue")
        moveToHome()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        loginFacebook.readPermissions = permissionsToRead
        let token = UserDefaults.standard.string(forKey: "token")
        if token != nil{
            graphRequestToReturnUserData(graphParameters: graphRequestParameters)
            //loginFacebook.isHidden = true
        }else{
            goSwipping.isHidden = true
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if ((error) != nil) {
            // Process error
            print("Error")
        }
        else if result.isCancelled {
            // Handle cancellations
            print("Cancelled")
        }
        else {
            // Navigate to other view
            print("Success")
            if result.grantedPermissions.contains("email") {
                //print(result.grantedPermissions)
                graphRequestToReturnUserData(graphParameters: graphRequestParameters)
                moveToHome()
            }
        }
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Out")
        UserDefaults.standard.set(nil, forKey: "token")
    }
    
    //Mark: GraphRequestForFetchingUserData
    func graphRequestToReturnUserData(graphParameters : Dictionary<String, String>) {
        FBSDKGraphRequest(graphPath: "me", parameters: graphParameters).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                print(fbDetails)
                print(FBSDKAccessToken.current().tokenString)
            }
        })
    }
    func moveToHome() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RootHomeVC") as! UINavigationController
        self.present(vc, animated: true, completion: nil)
    }
    
}

