//
//  ListPhotosViewController.swift
//  TinderClone
//
//  Created by admin on 5/10/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import UIKit
import FBSDKCoreKit
class ListPhotosViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: *** Local variables
    var delegateSetting: SettingsControllerDelegate?
    var idAlbum: String? = nil
    let graphRequestParameters = ["fields": "id,name,photos.limit(100){id,images},photo_count"]
    var alertController: UIAlertController? = nil
    var arrPhotoId: Array<String> = []
    var arrPhotoURL: Array<String> = []
    
    // MARK: *** Data Models
    
    // MARK: *** UI Elements
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: *** UI Events
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrPhotoId.removeAll()
        arrPhotoURL.removeAll()
        print(idAlbum!)
        graphRequestToReturnUserData(graphParameters: graphRequestParameters, idAlbum: idAlbum!)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToSettingVC" {
            let dest = segue.destination as! SettingsViewController
            dest.setImage(newImage: arrPhotoURL[selectedRow!])
            //dest.delegateListAlbum = self
        }
    }
    
    //Mark: GraphRequestForFetchingUserData
    func graphRequestToReturnUserData(graphParameters : Dictionary<String, String>, idAlbum: String) {
        showDialogWaiting()
        FBSDKGraphRequest(graphPath: idAlbum, parameters: graphParameters).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                let fbDetails = result as! NSDictionary
                let photo = fbDetails["photos"] as AnyObject
                let data = photo["data"] as! [AnyObject]

                for i in 0...data.count-1{
                    let idPhoto = data[i]["id"] as! String
                    let urlImages = data[i]["images"] as! [AnyObject]
                    let imageSource = urlImages[1]["source"] as! String
                    self.arrPhotoId.append(idPhoto)
                    self.arrPhotoURL.append(imageSource)
                }
                print("Count: " + String(self.arrPhotoURL.count))
                self.alertController?.dismiss(animated: true, completion: nil)
                
                self.collectionView.delegate = self
                self.collectionView.dataSource = self
                self.collectionView.reloadData()
            }
        })
    }
    func showDialogWaiting() {
        alertController = UIAlertController(title: nil, message: "Please wait\n\n", preferredStyle: .alert)
        let spinnerIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinnerIndicator.center = CGPoint(x: 135.0, y: 65.5)
        spinnerIndicator.color = UIColor.black
        spinnerIndicator.startAnimating()
        alertController?.view.addSubview(spinnerIndicator)
        self.present(alertController!, animated: false, completion: nil)
    }

    
    // MARK *** CollectionView
    var selectedRow : Int? = nil
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        print(indexPath.row)
        performSegue(withIdentifier: "segueToSettingVC", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPhotoId.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCollectionViewCell{
            //set image
            let url = URL(string: arrPhotoURL[indexPath.row])
            let data = try? Data(contentsOf: url!)
            let uiImage = UIImage(data: data!)
            cell.image.image = uiImage
            
            if indexPath.row == arrPhotoId.count-1{
                print("End")
            }
            return cell
        }else{
            return UICollectionViewCell()
        }
        
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.superview?.frame.size.width
        let imageWidth = width! * 0.285
        return CGSize(width: imageWidth, height: imageWidth)
    }
}
