//
//  UIImageExtension.swift
//  TinderClone
//
//  Created by admin on 5/9/17.
//  Copyright © 2017 nhp. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func textToImage(drawText text: NSString, inImage image: UIImage, atPoint point: CGPoint, withSize size: Float) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont(name: "Thonburi", size: CGFloat(size))!
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            ] as [String : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
